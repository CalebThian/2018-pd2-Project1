/*
#include <iostream>
#ifndef READMAP_H
#define READMAP_H
#include "readMap.h"
#endif
#ifndef GIVECOMMAND_H
#define GIVECOMMAND_H
#include "giveCommand.h"
#endif
using namespace std;

int main()
{
	Time TimeNow;
	cDeck deck;
	Tower tower[6];
	cFriend Friend;
	Enemy enemy;
	cout<<"2 9 3 6 7 8 5 C"<<"\n";
	creadMap Observer;
	giveCommand Commander;
	while(1)
	{
		Observer.startReadMap(TimeNow,deck,tower,Friend,enemy);
		Commander.printCommand(deck,enemy,Friend);
	}
	return 0;
}
*/
///*
#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;

int nMp;
char nCard[4];
//char nCard1;
//char nCard2;
//char nCard3;
//char nCard4;
int nTowerHP[6];
int friCounter;
int eneCounter;
char friID[50];
char eneID[50];
int friHP[50];
int eneHP[50];
int friCorx[50];
int friCory[50];
int eneCorx[50];
int eneCory[50];
int corx;
int cory;
string nStr;
string nInfoType;
int nTimeNow;
char nT1;
char nT2;
char nT3;
string at;
string comma;
string HP;
int abc=0;

void nGiveCommand();

void registerDeck() {
    cout<<"2 3 4 9 C 7 5 6"<<endl;
};

void funcReadMap() {
    //cout<<nStr<<endl;
    if(nStr=="BEGIN") {
        //d1:friCounter=0;
        //d1:eneCounter=0;
        while(cin>>nInfoType) { // && nInfoType!="END")
            //cout<<nInfoType<<" "<<nTimeNow<<endl;
            if(nInfoType=="TIME") {
                cin>>nTimeNow;
                //cin>>nT1>>nT2>>nT3;
                //cout<<nT1<<nT2<<nT3;
                //d2:cout<<nTimeNow<<endl;
            } else if(nInfoType == "MANA") {
                cin>>nMp;
                //d2:cout<<nMp<<endl;
            } else if(nInfoType == "DECK") {
                //nCard[0]=nCard[1]=nCard[2]=nCard[3]='0';
                //d2:cout<<"Deck is ran"<<endl;
                //d2:abc++;
                //d2:cout<<abc<<endl;
                cin>>nCard[0]>>nCard[1]>>nCard[2]>>nCard[3];

                //cin>>nCard1>>nCard2>>nCard3>>nCard4;
                /*
                nCard[0]=nCard1;
                nCard[1]=nCard2;
                nCard[2]=nCard3;
                nCard[3]=nCard4;
                */
                //d2:cout<<nCard[0]<<" "<<nCard[1]<<" "<<nCard[2]<<" "<<nCard[3]<<endl;
                //cout<<nCard1<<" "<<nCard2<<" "<<nCard3<<" "<<nCard4<<endl;
            } else if(nInfoType == "TOWER") {
                int i;
                cin>>i;
                cin>>nTowerHP[i-1];
            } else if(nInfoType == "FRIEND") {
                cin>>friID[friCounter];
                cin>>at;
                cin>>friCorx[friCounter]>>comma>>friCory[friCounter];
                cin>>HP>>friHP[friCounter];
                friCounter++;
            } else if(nInfoType == "ENEMY") {
                cin>>eneID[eneCounter];
                cin>>at;
                cin>>eneCorx[eneCounter]>>comma>>eneCory[eneCounter];
                cin>>HP>>eneHP[eneCounter];
                eneCounter++;
            }
            ///*d1:
            else if(nInfoType == "END") {
                nGiveCommand();
            }
            //*/
            else if(nInfoType== "BEGIN") {
                funcReadMap();
            } else {
                cout<<"Something is wrong with nInfoType"<<endl;
            };
        };
    } else {
    };
};


bool nCheckCor_fri(int x, int y) {
    for(int j=0; j< friCounter; j++) {
        if(x == friCorx[j] && y == friCory[j]) {
            return false;
        };
    };
    return true;
};

bool nCheckCor_ene(int x, int y) {
    for(int j=0; j< friCounter; j++) {
        if(x == friCorx[j] && y == friCory[j]) {
            return false;
        };
    };
    return true;
};


bool nCheckCor(int x, int y) {
    if(nCheckCor_fri(x,y)==true&&nCheckCor_ene(x,y)==true) {
        if(x==3||x==4||x==5||x==15||x==16||x==17) {
            if(y==4||y==5||y==6||y==7||y==8||y==9||y==10) {
                return false;
            } else {
                return true;
            };
        } else if(x==8||x==9||x==10) {
            if(y==3|y==4||y==5||y==6||y==7||y==8) {
                return false;
            } else {
                return true;
            };
        } else {
            return true;
        };
    } else {
        return false;
    };
};

void nGenCor(char a) {
    int nCorx;
    int nCory;
    srand(time(0));
    if(a=='2'||a=='9'||a=='5'||a=='C') {
        nCorx= rand()%2 + 8;
        nCory= rand()%3;
        if(nCheckCor(nCorx,nCory)==true) {
            corx=nCorx;
            cory=nCory;
        } else {
            nGenCor(a);
        };
    } else if(a=='3'||a=='4'||a=='7'||a=='6') {
        nCorx= rand()%21;
        nCory= rand()%22;
        if(nCheckCor(nCorx,nCory)==true) {
            corx=nCorx;
            cory=nCory;
        } else {
            nGenCor(a);
        };
    } else {
        cout<<"Non registered minion in nGenCor()"<<endl;
    };
};

bool nCheckDeck(char a) {
    if(nCard[0]==a||nCard[1]==a||nCard[2]==a||nCard[3]==a) {
        return true;
    } else {
        return false;
    };
};

void nCase0Func() {
    cout<<"0 Error"<<endl;
};

void nCase1Func() {
    if(nCheckDeck('5')==true) {
        nGenCor('5');
        cout<<"1 5 "<<corx<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else {
        cout<<'0'<<endl;
        //nCase0Func();
    };
};

void nCase2Func() {
    if(nCheckDeck('3')==true) {
        nGenCor('3');
        cout<<"1 3 "<<corx<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else {
        nCase1Func();
    };
};

void nCase3Func() {
    if(nCheckDeck('9')==true) {
        nGenCor('9');
        cout<<"1 9 "<<corx<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else if(nCheckDeck('2')==true) {
        nGenCor('2');
        cout<<"1 2 "<<corx<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else {
        nCase2Func();
    };
};

void nCase4Func() {
    if(nCheckDeck('6')==true) {
        nGenCor('6');
        cout<<"1 6 "<<corx<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else {
        nCase3Func();
    };
};

void nCase5Func() {
    cout<<"0"<<endl;
};

void nCase6Func() {
    if(nCheckDeck('9')==true && nCheckDeck('2')==true) {
        nGenCor('9');
        cout<<"1 9 "<<corx<<" "<<cory<<endl;
        nGenCor('2');
        cout<<"1 2 "<<corx+1<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else if(nCheckDeck('4')==true) {
        nGenCor('4');
        cout<<"1 4 "<<corx<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else if(nCheckDeck('6')==true && nCheckDeck('3')==true) {
        nGenCor('6');
        cout<<"1 6 "<<corx<<" "<<cory<<endl;
        nGenCor('3');
        cout<<"1 3 "<<corx+1<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else {
        nCase4Func();
    };
};

void nCase7Func() {
    if(nCheckDeck('C')==true) {
        nGenCor('C');
        cout<<"1 C "<<corx<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else {
        nCase6Func();
    };
};

void nCase8Func() {
    if(nCheckDeck('7')==true) {
        nGenCor('7');
        cout<<"1 7 "<<corx<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else {
        nCase7Func();
    };
};

void nCase9Func() {
    if(nCheckDeck('4')==true && nCheckDeck('9')==true) {
        nGenCor('4');
        cout<<"1 4 "<<corx<<" "<<cory<<endl;
        nGenCor('9');
        cout<<"1 9 "<<corx+1<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else if(nCheckDeck('7')==true && nCheckDeck('5')==true) {
        nGenCor('7');
        cout<<"1 7 "<<corx<<" "<<cory<<endl;
        nGenCor('5');
        cout<<"1 5 "<<corx<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else if(nCheckDeck('4')==true && nCheckDeck('2')==true) {
        nGenCor('4');
        cout<<"1 4 "<<corx<<" "<<cory<<endl;
        nGenCor('2');
        cout<<"1 2 "<<corx+1<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else {
        nCase8Func();
    };
};

void nCase10Func() {
    if(nCheckDeck('7')==true && nCheckDeck('3')==true) {
        nGenCor('7');
        cout<<"1 7 "<<corx<<" "<<cory<<endl;
        nGenCor('3');
        cout<<"1 3 "<<corx+1<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else if(nCheckDeck('4')==true && nCheckDeck('6')==true) {
        nGenCor('4');
        cout<<"1 4 "<<corx<<" "<<cory<<endl;
        nGenCor('6');
        cout<<"1 6 "<<corx+1<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else if(nCheckDeck('C')==true && nCheckDeck('9')==true) {
        nGenCor('C');
        cout<<"1 C "<<corx<<" "<<cory<<endl;
        nGenCor('9');
        cout<<"1 9 "<<corx+1<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else if(nCheckDeck('C')==true && nCheckDeck('2')==true) {
        nGenCor('C');
        cout<<"1 C "<<corx<<" "<<cory<<endl;
        nGenCor('2');
        cout<<"1 2 "<<corx+1<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else if(nCheckDeck('4')==true && nCheckDeck('9')==true && nCheckDeck('5')==true) {
        nGenCor('4');
        cout<<"1 4 "<<corx<<" "<<cory<<endl;
        nGenCor('9');
        cout<<"1 9 "<<corx+1<<" "<<cory<<endl;
        nGenCor('5');
        cout<<"1 5 "<<corx+2<<" "<<cory<<endl;
        cout<<"0"<<endl;
    } else {
        nCase9Func();
    };
};

void nGiveCommand() {
    switch(nMp) {
    /*
    case 0:
    	nCase0Func();
    	break;
    	*/
    case 1:
        nCase1Func();
        break;
    case 2:
        nCase2Func();
        break;
    case 3:
        nCase3Func();
        break;
    case 4:
        nCase4Func();
        break;
    case 5:
        nCase5Func();
        break;
    case 6:
        nCase6Func();
        break;
    case 7:
        nCase7Func();
        break;
    case 8:
        nCase8Func();
        break;
    case 9:
        nCase9Func();
        break;
    case 10:
        nCase10Func();
        break;
    //
    default:
        break;
    };
    funcReadMap();
};

int main() {
    cout<<"2 3 4 9 C 7 5 6"<<endl;
//	while(cin>>nStr &&(nInfoType !="You Win"||nInfoType !="You Lose"||nInfoType != "Player 1 fault") )
    cin>>nStr;
    if(nStr=="BEGIN") {
        funcReadMap();
        nGiveCommand();
        //d2:
        //cout<<nCard[0]<<" "<<nCard[1]<<" "<<nCard[2]<<" "<<nCard[3]<<endl;
    };
}

// */
