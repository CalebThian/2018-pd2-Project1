#include <iostream>
#ifndef DECK_H
#define DECK_H
#include "Deck.h"
#endif
using namespace std;

void cDeck::printDeckUsable() {
    for(int i=0; i<4; i++) {
        cout<<card[i]<<"\n";
    };
};

void cDeck::createManaCostArray() {
    for(int i=0; i<4; i++) {
        if(card[i]=='1') {
            ManaCost[i] = 5;
        } else if(card[i]=='2') {
            ManaCost[i] = 3;
        } else if(card[i]=='3') {
            ManaCost[i] = 2;
        } else if(card[i]=='4') {
            ManaCost[i] = 6;
        } else if(card[i]=='5') {
            ManaCost[i] = 1;
        } else if(card[i]=='6') {
            ManaCost[i] = 4;
        } else if(card[i]=='7') {
            ManaCost[i] = 8;
        } else if(card[i]=='8') {
            ManaCost[i] = 5;
        } else if(card[i]=='9') {
            ManaCost[i] = 3;
        } else if(card[i]=='C') {
            ManaCost[i] = 7;
        } else {
            cout<<"There is an unidentified minion\n";
        };
    };
};

void cDeck::determineCallable() {
    for(int i=0; i<4; i++) {
        if(ManaCost[i]<mp||ManaCost[i]==mp) {
            Callable[i]=true;
        } else if(ManaCost[i]>mp) {
            Callable[i]=false;
        } else {
            cout<<"Error:determineCallable() has an error occured.\n";
        };
    };
};

void cDeck::determineUsableMinion() {
    int j=0;
    for(int i=0; i<4; i++) {
        if(Callable[i]==true) {
            UsableMinion[i-j]=card[i];
        } else if (Callable[i]==false) {
            j++;
        } else {
            cout<<"Error:determineUsableMinion() has an error occured.\n";
        };
    };
    j++;
};

bool cDeck::checkUsableMinion(char a) {
    if(card[0]==a) {
        card[0]=='0';
        return true;
    } else if(card[1]==a) {
        card[1]=='0';
        return true;
    } else if(card[2]==a) {
        card[2]=='0';
        return true;
    } else if(card[3]==a) {
        card[3]=='0';
        return true;
    } else {
        return false;
    };
    /*int c=sizeof(UsableMinion)/sizeof(UsableMinion[0]);
    cout<<c<<endl;
    char b='0';
    for(int i=0;i<c;i++)
    {
    	if(UsableMinion[i]==a)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	};
    };
    */
};

char cDeck::case1Func() {
    if(checkUsableMinion('5')==true) {
        return '5';
    } else {
        //	cout<<mp<<endl;
    };
};

char cDeck::case2Func() {
    if(checkUsableMinion('3')==true) {
        return '3';
    } else {
        case1Func();
    }
};

char cDeck::case3Func() {
    if(checkUsableMinion('9')==true) {
        return '9';
    } else if(checkUsableMinion('2')==true) {
        return '2';
    } else {
        case2Func();
    };
};

char cDeck::case4Func() {
    if(checkUsableMinion('6')==true) {
        return '6';
    } else {
        case3Func();
    };
};

char cDeck::case5Func() {
    if(checkUsableMinion('8')==true) {
        return '8';
    } else {
        case4Func();
    };
};

char cDeck::case6Func() {
    if(checkUsableMinion('9')==true&&checkUsableMinion('2')==true) {
        return 'P';
    } else if(checkUsableMinion('3')==true&&checkUsableMinion('6')==true) {
        return 'Q';
    } else {
        case5Func();
    };
};

char cDeck::case7Func() {
    if(checkUsableMinion('C')==true) {
        return 'C';
    } else {
        case6Func();
    };
};

char cDeck::case8Func() {
    if(checkUsableMinion('7')==true) {
        return '7';
    } else {
        case7Func();
    };
};

char cDeck::case9Func() {
    if(checkUsableMinion('8')==true&&checkUsableMinion('5')==true) {
        return 'R';
    } else {
        case8Func();
    };
};

char cDeck::case10Func() {
    if(checkUsableMinion('8')==true&&checkUsableMinion('3')==true) {
        return 'S';
    } else {
        case9Func();
    };
};

char cDeck::determinePutMinion() {
    switch(mp) {
    case 1:
        return case1Func();
        break;
    case 2:
        return case2Func();
        break;
    case 3:
        return case3Func();
        break;
    case 4:
        return case4Func();
        break;
    case 5:
        return case5Func();
        break;
    case 6:
        return case6Func();
        break;
    case 7:
        return case7Func();
        break;
    case 8:
        return case8Func();
        break;
    case 9:
        return case9Func();
        break;
    case 10:
        return case10Func();
        break;
    default:
        cout<<"SOMETHING IS WRONG WITH MANA";
    };
};

char cDeck::overall() {
    //createManaCostArray();
    //determineCallable();
    //determineUsableMinion();
    return determinePutMinion();
};
