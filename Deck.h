#include <iostream>

using namespace std;

class cDeck {
  public:
    int mp;
    char card[4];
    void printDeckUsable();
    char overall();
  private:
    void createManaCostArray();
    void determineCallable();
    void determineUsableMinion();
    char determinePutMinion();
    int  ManaCost[4];
    bool Callable[4];
    char UsableMinion[];
    bool checkUsableMinion(char a);
    char case1Func();
    char case2Func();
    char case3Func();
    char case4Func();
    char case5Func();
    char case6Func();
    char case7Func();
    char case8Func();
    char case9Func();
    char case10Func();
};

