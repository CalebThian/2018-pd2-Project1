#include <iostream>
#include <string>
#ifndef TIME_H
#define TIME_H
#include "Time.h"
#endif
#ifndef TOWER_H
#define TOWER_H
#include "Tower.h"
#endif
#ifndef DECK_H
#define DECK_H
#include "Deck.h"
#endif
#ifndef FRIEND_H
#define FRIEND_H
#include "Friend.h"
#endif
#ifndef ENEMY_H
#define ENEMY_H
#include "Enemy.h"
#endif

class creadMap {
  public:
    string null;
    string str;
    string infoType;
    string information;
    string nothing;
    int i;
    int a;
    int b;
    int yf;
    int ye;
    void readTime(Time &TimeNow);
    void readMana(cDeck &deck);
    void readDeck(cDeck &deck);
    void readTower(Tower tower[6]);
    void readFriend(cFriend &Friend);
    void readEnemy(Enemy &enemy);
    void readEnd();
    void startReadMap(Time &TimeNow,cDeck &deck,Tower tower[6],cFriend &Friend,Enemy &enemy);
    int end();
};
